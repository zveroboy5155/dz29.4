#include <iostream>
#include <DZ29.4.h>

int main() {
	int cmd = -1;

	while (cmd != 0) {
		std::cout << "Enter task number( 1 or 2) or 0 for exit" << std::endl;
		std::cin >> cmd;
		if (cmd == 1) task1();
		else if (cmd == 2) task2();
	}
    
    return 0;
}
