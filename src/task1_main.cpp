#include "DZ29.4.h"

class Talant{
public:
    virtual void print() = 0;
};

class Swimming : public Talant{
public:
    virtual void print(){
        std::cout << " It can \"Swimming\"" <<std::endl;
    }
};

class Dancing : public Talant{
public:
    virtual void print(){
        std::cout << " It can \"Dancing\"" <<std::endl;
    }
};

class Counting : Talant{
public:
    virtual void print(){
        std::cout << " It can \"Counting\"" <<std::endl;
    }
};

class Dog{

    std::vector <Talant*> talants;

    std::string name;
public:
    Dog(std::string newName){
        name = newName;
        talants.push_back(new Dancing);
        talants.push_back(new Swimming);
    }
    void show_talents(){
        std::cout << "This is " << name << " and it has some talents: " <<std::endl;
        for(auto it: talants) it->print();
    }
};

void task1(){
    Dog d("Sharik");

    d.show_talents();

}