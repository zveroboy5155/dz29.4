#include "DZ29.4.h"
#include <thread>
#include <chrono>

class Shape;
class Circle;
class Triangle;
class Rectangl;
struct BoundingBoxDimensions{
    double len, with;
};

class Shape {
protected:
    int x_pos, y_pos;
public:
    virtual double square()  = 0;
    virtual BoundingBoxDimensions dimensions()  = 0;
    virtual std::string type() = 0;
	
};
//
class Circle: public Shape {
    double rad;
public:
    Circle() {
        std::cout << "  " << std::endl;
        std::cin >> rad;
    }
    double square() {
        return atan(1) * 4 * rad;
    }
    BoundingBoxDimensions dimensions() {
		BoundingBoxDimensions bond;
		bond.with = rad;
		bond.len = rad;
		return bond;
    };
};
//
class Triangle: public Shape {
    double a,b,c;
public:
    Triangle(double a, double b, double c) {
		this->a = a;
		this->b = b;
		this->c = c;
    }
    double square() {
		double p;

		p = (a + b + c)/2;
		return sqrt(p * (p - a) * (p - b) * (p - c));
    }
    BoundingBoxDimensions dimensions() {
        BoundingBoxDimensions bond;
		double p, sLen;

		p = (a + b + c) / 2;
		sLen = a * b * c / (4 * sqrt(p*(p - a)*(p - b)*(p - c))) * 2;
		bond.with = sLen;
        bond.len = sLen;
        return bond;
    }
	std::string type() { return "Triangle"; }
};
//
class Rectangle: public Shape {
    double len, with;
public:
    Rectangle() {
        std::cout << "   " << std::endl;
        std::cin >> this->len >> this->with;
    }
    double square() {
        return len * with;
    }
    BoundingBoxDimensions dimensions() {
        BoundingBoxDimensions bond;
        bond.with = with;
        bond.len = len;
        return bond;
    };
	std::string type() { return "Rectangle"; }
};

void printParams(Shape *shape) {
	BoundingBoxDimensions curBond;

	std::cout << "Type: " << shape->type() << std::endl;
	std::cout << "Square: " << shape->square() << std::endl;

	curBond = shape->dimensions();
	std::cout << "Width: " << curBond.with << std::endl;
	std::cout << "Heigth: " << curBond.len << std::endl;
}

void task2() {
	Triangle t(3, 4, 5);
	printParams(&t);
	
	//std::this_thread::sleep_for(std::chrono::seconds(2));
}